#!/usr/bin/env python2
import requests
import random
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


def hit_blogger_by_requests(lines, url_list):
    for url in url_list:
        proxy = random.choice(lines[:100])
        proxies = {"http": "http://{proxy}".format(proxy=proxy.strip())}
        x = requests.get(url, proxies=proxies)
        print(x.text, x.status_code)


def hit_blogger_by_selenium(lines, url_list):
    binary = FirefoxBinary('/usr/bin/firefox')

    for url in url_list:
        proxy = random.choice(lines)
        proxies = {"http": "http://{proxy}".format(proxy=proxy.strip())}
        browser = webdriver.Firefox(firefox_binary=binary, proxy=proxies)
        browser.get(url)


def hit_blogger_by_headless_selenium(lines, url_list):
    binary = FirefoxBinary('/usr/bin/firefox')
    options = webdriver.FirefoxOptions()
    options.add_argument('--headless')

    for url in url_list:
        proxy = random.choice(lines)
        proxies = {"http": "http://{proxy}".format(proxy=proxy.strip())}
        browser = webdriver.Firefox(firefox_binary=binary, options=options,
                                    proxy=proxies)
        browser.get(url)
        browser.close()


def main_call():
    f = open("/home/jai/hit_blogger/Proxy_List.txt")
    lines = f.readlines()
    url_list = [
        'https://djgoo.blogspot.com/2019/12/how-to-delete-perticular-type-files.html',
        "https://djgoo.blogspot.com/2019/12/implementaion-of-autocomplete-in-django.html",
        "https://djgoo.blogspot.com/2019/12/how-to-handle-django-url-having-in.html",
        "https://djgoo.blogspot.com/2019/11/indent-python-project-directory.html",
        "https://djgoo.blogspot.com/2019/12/headless-selenium-with-python.html"
        ]
    # hit_blogger_by_requests(lines, url_list)
    # hit_blogger_by_selenium(lines, url_list)
    hit_blogger_by_headless_selenium(lines, url_list)


if __name__ == "__main__":
    main_call()
